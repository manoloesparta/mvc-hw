import data
from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def main_page():
    posts = data.posts
    return render_template('index.html', input_data=posts, title_page='random posts hahaha')

if __name__ == '__main__':
    app.run(debug=True)